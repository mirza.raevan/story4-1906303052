from django.test import TestCase
from django.urls import reverse
from .models import Forms1,Forms2,Forms3
from .forms import Forms_band,Forms_coding,Forms_sepeda

class UnitTest(TestCase):
    def test_root_url_status_200(self):
        response = self.client.get(reverse('Forms:kegiatan'))
        self.assertEqual(response.status_code, 200)

    def test_template_yang_digunakan_dari_halaman_kegiatan(self):
        response = self.client.get(reverse('Forms:kegiatan'))
        self.assertTemplateUsed(response, 'kegiatan.html')

    def test_isi_dari_halaman_kegiatan(self):
        response = self.client.get(reverse('Forms:kegiatan'))
        isi = response.content.decode('utf8')
        self.assertIn("Ngeband", isi)
        self.assertIn("Coding", isi)
        self.assertIn("Sepedaan", isi)
        self.assertIn("<input type ='submit' name = 'btn_form_band' value='Tambah Peserta'/>",isi)
        self.assertIn("<input type ='submit' name = 'btn_form_coding' value='Tambah Peserta'/>",isi)
        self.assertIn("<input type ='submit' name = 'btn_form_sepeda' value='Tambah peserta'/>",isi)
        self.assertIn('<form method="POST" action = "" id="form_band" style="margin-top: 20px;">' , isi)
        self.assertIn('<form method="POST" action = "" id="form_coding" style="margin-top: 20px;">' , isi)
        self.assertIn('<form method="POST" action = "" id="form_sepeda" style="margin-top: 20px;">' , isi)
        self.assertIn("Nama", isi)

    def test_models_dari_halaman_kegiatan(self):
        Forms1.objects.create(Nama='abc')
        n_band = Forms1.objects.all().count()
        self.assertEqual(n_band,1)
        Forms2.objects.create(Nama='abc')
        n_coding = Forms2.objects.all().count()
        self.assertEqual(n_coding,1)
        Forms3.objects.create(Nama='abc')
        n_sepeda = Forms3.objects.all().count()
        self.assertEqual(n_sepeda,1)

    def test_form_band_post_valid(self):
        response_post = self.client.post('/kegiatan', {'Nama':'abc', 'btn_form_band': 'Tambah Peserta'})
        self.assertEqual(response_post.status_code,200)
        form_band = Forms_band(data={'Nama':"abc"})
        self.assertTrue(form_band.is_valid())
        self.assertEqual(form_band.cleaned_data['Nama'],"abc")
    
    def test_form_coding_post_valid(self):
        response_post = self.client.post('/kegiatan', {'Nama':'abc', 'btn_form_coding': 'Tambah Peserta'})
        self.assertEqual(response_post.status_code,200)
        form_coding = Forms_coding(data={'Nama':"abc"})
        self.assertTrue(form_coding.is_valid())
        self.assertEqual(form_coding.cleaned_data['Nama'],"abc")

    def test_form_sepeda_post_valid(self):
        response_post = self.client.post('/kegiatan', {'Nama':'abc', 'btn_form_sepeda': 'Tambah Peserta'})
        self.assertEqual(response_post.status_code,200)
        form_sepeda = Forms_sepeda(data={'Nama':"abc"})
        self.assertTrue(form_sepeda.is_valid())
        self.assertEqual(form_sepeda.cleaned_data['Nama'],"abc")
        
        

    
        
        


    



