from django.urls import path

from . import views

app_name = 'main'

urlpatterns = [
    path('', views.profile, name='profile'),
    path('hobby', views.hobby, name='hobby'),
    path('experience', views.experience, name='experience'),
    path('socmed', views.socmed, name='socmed'),
    path('addjadwal' , views.addjadwal , name='addjadwal'),
    path('jadwal/<str:matkul>' , views.detailjadwal , name = 'detailjadwal'),
    path('jadwal', views.jadwal, name='jadwal'),
    path('delete/<str:matkul>',views.deletejadwal , name = 'deletejadwal'),
    
]
