from django.test import LiveServerTestCase, TestCase, tag
from django.urls import reverse
from selenium import webdriver


# @tag('functional')
# class FunctionalTestCase(LiveServerTestCase):
#     """Base class for functional test cases with selenium."""

#     @classmethod
#     def setUpClass(cls):
#         super().setUpClass()
#         # Change to another webdriver if desired (and update CI accordingly).
#         options = webdriver.chrome.options.Options()
#         # These options are needed for CI with Chromium.
#         options.headless = True  # Disable GUI.
#         options.add_argument('--no-sandbox')
#         options.add_argument('--disable-dev-shm-usage')
#         cls.selenium = webdriver.Chrome(options=options)

#     @classmethod
#     def tearDownClass(cls):
#         cls.selenium.quit()
#         super().tearDownClass()


# class TestUrls(TestCase):
#     def test_root_url_status_200(self):
#         response = self.client.get(reverse('main:profile'))
#         self.assertEqual(response.status_code, 200)
#     def test_hobby_url_status_200(self):
#         response = self.client.get(reverse('main:hobby'))
#         self.assertEqual(response.status_code, 200)
#     def test_experience_url_status_200(self):
#         response = self.client.get(reverse('main:experience'))
#         self.assertEqual(response.status_code, 200)
#     def test_socmed_url_status_200(self):
#         response = self.client.get(reverse('main:socmed'))
#         self.assertEqual(response.status_code, 200)
#     def test_add_jadwal_url_status_200(self):
#         response = self.client.get(reverse('main:addjadwal'))
#         self.assertEqual(response.status_code, 200)
#     def test_jadwal_url_status_200(self):
#         response = self.client.get(reverse('main:jadwal'))
#         self.assertEqual(response.status_code, 200)
#     def test_detail_jadwal_url_status_200(self):
#         response = self.client.get(reverse('main:detailjadwal'))
#         self.assertEqual(response.status_code, 200)
#     def test_delete_jadwal_url_status_200(self):
#         response = self.client.get(reverse('main:deletejadwal'))
#         self.assertEqual(response.status_code, 200)






# class MainFunctionalTestCase(FunctionalTestCase):
#     def test_root_url_exists(self):
#         self.selenium.get(f'{self.live_server_url}/')
#         html = self.selenium.find_element_by_tag_name('html')
#         self.assertNotIn('not found', html.text.lower())
#         self.assertNotIn('error', html.text.lower())
