from django import forms
from .models import Jadwal

class formjadwal(forms.ModelForm):
    class Meta:
        model = Jadwal
        fields = [
            'matkul',
            'dosen',
            'sks',
            'deskripsi',
            'semester',
            'kelas',
        ]