from django.contrib import admin

from .models import Forms1,Forms2,Forms3
admin.site.register(Forms1)
admin.site.register(Forms2)
admin.site.register(Forms3)
