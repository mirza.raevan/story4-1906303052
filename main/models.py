from django.db import models

# Create your models here.
class Jadwal (models.Model):
    matkul = models.CharField(max_length=50)
    dosen = models.CharField(max_length=50)
    sks = models.IntegerField()
    deskripsi = models.CharField (max_length=100)
    semester = models.IntegerField ()
    kelas = models.CharField(max_length=20)
