from django.shortcuts import render
from django.shortcuts import redirect
from .forms import formjadwal
from .models import Jadwal


def profile(request):
    return render(request, 'main/Story3.html')

def hobby(request):
    return render(request, 'main/Story3page2.html')

def experience(request):
    return render(request, 'main/Story3page3.html')

def socmed(request):
    return render(request, 'main/Story3page4.html')

def addjadwal(request):
    form = formjadwal()
    context = {
        'form':form
    }
    return render(request, 'main/AddForm.html' , context)

def jadwal(request):
    if request.method == 'POST':

        form = formjadwal(request.POST)
        if form.is_valid():
            form.save()
    daftarjadwal = Jadwal.objects.all()
    return render(request, 'main/Form.html' , {"daftarjadwal" : daftarjadwal})

def detailjadwal(request , matkul):
    jadwal = Jadwal.objects.get(matkul=matkul)
    return render(request, 'main/DetailJadwal.html' , {"jadwal" : jadwal})
        
def deletejadwal(request , matkul):
    item = Jadwal.objects.get(matkul=matkul)
    item.delete()
    return redirect("/jadwal")    
    