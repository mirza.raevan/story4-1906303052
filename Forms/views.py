from django.shortcuts import render
from .forms import Forms_band,Forms_coding,Forms_sepeda
from .models import Forms1,Forms2,Forms3

# Create your views here.
def kegiatan(request):
    if request.method == 'POST' and 'btn_form_band' in request.POST:
        form_band = Forms_band(request.POST)
        if form_band.is_valid():
            form_band.save()
    elif request.method == 'POST' and 'btn_form_coding' in request.POST:
        form_coding = Forms_coding(request.POST)
        if form_coding.is_valid():
            form_coding.save()
    elif request.method == 'POST' and 'btn_form_sepeda' in request.POST:
        form_sepeda = Forms_sepeda(request.POST)
        if form_sepeda.is_valid():
            form_sepeda.save()
    
    form_band = Forms_band()
    form_coding = Forms_coding()
    form_sepeda = Forms_sepeda()
    peserta_band = Forms1.objects.all()
    peserta_coding = Forms2.objects.all()
    peserta_sepeda = Forms3.objects.all()
    context = {
        'list_peserta_band': peserta_band,
        'list_peserta_coding':peserta_coding,
        'list_peserta_sepeda':peserta_sepeda,
        'form_band':form_band,
        'form_coding':form_coding,
        'form_sepeda':form_sepeda,
        }
    return render(request, 'kegiatan.html' ,context)

