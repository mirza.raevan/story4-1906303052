from django import forms
from .models import Forms1,Forms2,Forms3

class Forms_band(forms.ModelForm):
    class Meta:
        model = Forms1
        fields = [
            'Nama',
        ]

class Forms_coding(forms.ModelForm):
    class Meta:
        model = Forms2
        fields = [
            'Nama',
        ]

class Forms_sepeda(forms.ModelForm):
    class Meta:
        model = Forms3
        fields = [
            'Nama',
        ]    