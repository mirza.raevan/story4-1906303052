$(function() {
    $(".accordion").accordion({
        collapsible: true,
        active: false,
    });
    

    $(".arrow-down").click(function(){
        event.stopPropagation();
        var accordion=$(this).parent().parent();
        accordion.insertAfter(accordion.next());
    });
    $(".arrow-up").click(function(){
        event.stopPropagation();
        var accordion=$(this).parent().parent();
        accordion.insertBefore(accordion.prev());
    });
});

