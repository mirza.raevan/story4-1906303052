from django.urls import path
from . import views
app_name = 'Forms'

urlpatterns = [
    path('kegiatan', views.kegiatan, name='kegiatan')
]