from django.test import TestCase, Client
from django.urls import reverse

class UnitTest (TestCase):
    def test_url_200 (self):
        response = Client().get("/searchbooks")
        self.assertEqual(response.status_code,200)

    def test_template (self):
        response = Client().get('/searchbooks')
        self.assertTemplateUsed(response,'searchbooks.html')

    def test_json_url(self):
        response = Client().get('/searchbooks/data')
        self.assertEqual(response.status_code,200)

    def test_html_content (self):
        response = self.client.get(reverse('searchbooks:searchbooks'))
        isi = response.content.decode('utf8')
        self.assertIn("Book List" , isi)
        self.assertIn("No", isi)
        self.assertIn("Cover", isi)
        self.assertIn("Title", isi)
        self.assertIn("Author", isi)
        self.assertIn("Publisher", isi)
        self.assertIn("Published Date", isi)



